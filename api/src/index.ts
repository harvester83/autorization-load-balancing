import { createConnection } from 'typeorm'
import { ApolloServer } from 'apollo-server-express';
import { typeDefs } from './typeDefs';
import { resolvers } from './resolvers';

import express = require('express');
import session = require('express-session');

const redisHost = process.env.REDIS_HOST;

const redis = require('redis');
const redisClient = redis.createClient(6379, redisHost);
const redisStore = require('connect-redis')(session);

const app = express();

app.use(session({
  secret: 'keyboard cat',
  cookie: { path: '/', domain: '.local', httpOnly: true, secure: false, sameSite: true },
  store: new redisStore({ host: redisHost, port: 6379, client: redisClient, ttl: 86400 }),
}));

const startServer = async () => {
  const server = new ApolloServer({
    typeDefs,
    resolvers,
    context: ({ req }: any) => ({ req }),
  });

  await createConnection();

  server.applyMiddleware({
    app,
    cors: {
      credentials: true,
      origin: 'http://web.local',
    },
  });

  app.listen({ port: 80 }, () => {
    console.log(`🚀 Server ready at http://api.local:80${server.graphqlPath}`);
  });
};

startServer();
