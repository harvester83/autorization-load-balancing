import * as React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { RegisterView } from './modules/user/RegisterView';
import { LoginView } from './modules/user/LoginView';
import { MeView } from './modules/user/MeView';

const Routes = () => {
  return (
    <BrowserRouter>
      <h1>Header</h1>
      <Switch>
        <Route path="/register" component={RegisterView} />
        <Route path="/login" component={LoginView} />
        <Route path="/me" component={MeView} />
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
